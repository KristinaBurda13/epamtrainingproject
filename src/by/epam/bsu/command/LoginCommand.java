package by.epam.bsu.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.logic.IssurePageLogic;
import by.epam.sec.logic.LoginLogic;
import by.epam.sec.resourse.ConfigurationManager;
import by.epam.sec.resourse.MessageManager;

public class LoginCommand implements ActionCommand {
	private static final String PARAM_NAME_LOGIN = "login";
	private static final String PARAM_NAME_PASSWORD = "password";
	private static final String PARAM_NAME_USER = "user";
	private static final String PARAM_NAME_CITIES = "cities";
	private static final String PARAM_NAME_ADMIN = "admin";
	private static final String PARAM_NAME_ISSURES = "issures";
	private static final String PARAM_NAME_ROLE = "role";
	private static final String PAGE_FOR_USER = "path.page.main";
	private static final String PAGE_FOR_ADMIN = "path.page.issures";
	private static final String ATTRIBUTE_ERROR = "errorLoginPassMessage";
	private static final String MESSAGE_ERROR = "message.loginerror";
	private static final String PAGE_LOGIN = "path.page.login";
	

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse respons) {
		String page = null;

		String login = request.getParameter(PARAM_NAME_LOGIN);
		String pass = request.getParameter(PARAM_NAME_PASSWORD);
		if (LoginLogic.checkLogin(login, pass)) {
			request.getSession().setAttribute(PARAM_NAME_USER, login);
			request.setAttribute(PARAM_NAME_CITIES, LoginLogic.returnCities());

			if (!LoginLogic.isAdmin()) {
				request.getSession().setAttribute(PARAM_NAME_ROLE, PARAM_NAME_USER);
				page = ConfigurationManager.getProperty(PAGE_FOR_USER);

			} else {
				request.getSession().setAttribute(PARAM_NAME_ROLE, PARAM_NAME_ADMIN);
				page = ConfigurationManager.getProperty(PAGE_FOR_ADMIN);
				request.setAttribute( PARAM_NAME_ISSURES, IssurePageLogic.retrunIssures());

			}

		} else {
			request.setAttribute(ATTRIBUTE_ERROR,
					MessageManager.getProperty(MESSAGE_ERROR));
			page = ConfigurationManager.getProperty(PAGE_LOGIN);
		}
		return page;
	}

	@Override
	public boolean changePage(HttpServletResponse response,
			HttpServletRequest request) {

		return false;

	}
}