package by.epam.bsu.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.sec.resourse.ConfigurationManager;
import by.epam.sec.resourse.MessageManager;
import by.epam.sec.action.ActionCommand;
import by.epam.sec.entity.Room;
import by.epam.sec.logic.LoginLogic;
import by.epam.sec.logic.MainPageLogic;

public class MainCommand implements ActionCommand {
	private static final String PAGE_CHOOSE="path.page.choose";
	private static final String PARAMETR_MENU="menu";
	private static final String PARAMETR_IN_DATE="inDate";
	private static final String PARAMETR_OUT_DATE="outDate";
	private static final String PARAMETR_MENU1="menu1";
	private static final String ATTRIBUTE_ROOMS="suitable_rooms";

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse respons) {
		

		String page = ConfigurationManager.getProperty(PAGE_CHOOSE);
		String city = (String) request.getParameter(PARAMETR_MENU);
		int persons = Integer.parseInt(request.getParameter(PARAMETR_MENU1));
		ArrayList<Room> listRooms = MainPageLogic.returnRooms(persons, 2, city);
		List<Map<String,Object>> dataList= new ArrayList<Map<String,Object>>();

		for (int i = 0; i < listRooms.size(); i++) {
			dataList.add(listRooms.get(i).getMap());
		}
		request.setAttribute(ATTRIBUTE_ROOMS, dataList);
		return page;
	}

	@Override
	public boolean changePage(HttpServletResponse response,
			HttpServletRequest request) {
		
		return false;
	}
}
