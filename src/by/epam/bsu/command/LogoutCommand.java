package by.epam.bsu.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.resourse.ConfigurationManager;

public class LogoutCommand implements ActionCommand {
	final static private String PAGE = "/index.jsp";
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse respons) {
		String page = ConfigurationManager.getProperty(PAGE);
		request.getSession().invalidate();
		return page;
	}

	@Override
	public boolean changePage(HttpServletResponse response, HttpServletRequest request) {
		
		return false;
		
	}
}