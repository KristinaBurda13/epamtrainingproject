package by.epam.bsu.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.logic.ChooseRoomLogic;
import by.epam.sec.resourse.ConfigurationManager;

public class ChooseRoomCommand implements ActionCommand {
	final static private String TITLE_OF_THE_HOTEL="title_of_choosen_hotel";
	final static private String ID_ROOM="id_of_choosen_room";
	final static private String FINAL_INFO="final_info";
	final static private String PAGE="path.page.makeIssures";
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse respons) {
		
		String page = ConfigurationManager.getProperty(PAGE);
		String hotelTitle=request. getParameter(TITLE_OF_THE_HOTEL);
		int id_room=Integer.parseInt(request. getParameter(ID_ROOM));
		request.setAttribute(FINAL_INFO, ChooseRoomLogic.returnRoomById(id_room, hotelTitle));
		return page;
	}

	@Override
	public boolean changePage(HttpServletResponse response, HttpServletRequest request) {
		return false;
	}
}
