package by.epam.bsu.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import by.epam.sec.action.ActionCommand;

public class ChangeLanguageCommand implements ActionCommand {
    final static private String LANGUAGE ="lang";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse respons) {
		return null;
	}

	@Override
	public boolean changePage(HttpServletResponse response, HttpServletRequest request) {
		
		String lang=request.getParameter(LANGUAGE );
		request.getSession().setAttribute(LANGUAGE , lang);
		return true;
	}
}
