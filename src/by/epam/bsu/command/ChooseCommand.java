package by.epam.bsu.command;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.logic.ChooseRoomLogic;

public class ChooseCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ChooseCommand.class);
	final static private String ID_OF_THE_ROOM="id_room";
	final static private String TITLE_OF_THE_HOTEL="hotel_title";
	final static private String TYPE="application/json";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse respons) {
		return null;
	}

	@Override
	public boolean changePage(HttpServletResponse response,HttpServletRequest request) {
		response.setContentType(TYPE);
		System.out.println(ChooseRoomLogic.returnRoomById(Integer.parseInt(request.getParameter(ID_OF_THE_ROOM)), request.getParameter(TITLE_OF_THE_HOTEL)));
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			
			LOG.error("Can not send json string, Error:"+e);
		}
		out.print(ChooseRoomLogic.returnRoomById(Integer.parseInt(request.getParameter(ID_OF_THE_ROOM)), request.getParameter(TITLE_OF_THE_HOTEL)).toJSONString());
		out.flush();
		return true;
	}
}
