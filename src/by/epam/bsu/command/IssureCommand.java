package by.epam.bsu.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.logic.IssurePageLogic;
import by.epam.sec.resourse.ConfigurationManager;

public class IssureCommand implements ActionCommand {
	private static final String ISSURES = "issures";
	private static final String PAGE_CHOOSE="path.page.choose";

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse respons) {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		String page = ConfigurationManager.getProperty(PAGE_CHOOSE);
		dataList.addAll(IssurePageLogic.retrunIssures());
		request.setAttribute(ISSURES, dataList);
		return page;
	}

	@Override
	public boolean changePage(HttpServletResponse response,
			HttpServletRequest request) {

		return false;

	}

}
