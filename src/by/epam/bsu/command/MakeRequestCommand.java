package by.epam.bsu.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.logic.AddIssureLogic;
import by.epam.sec.resourse.ConfigurationManager;

public class MakeRequestCommand implements ActionCommand {
	final static private String EMAIL="inputEmail";
	final static private String NAME="issureName";
	final static private String SERNAME="issureSerName";
	final static private String CARD="issurePasport";
	final static private String ISSADD="isAddIssure";
	final static private String IDHOTEL="id_hotel";
	final static private String IDROOM="id_room";
	final static private String MENU="menu1";
	final static private String USER="user";
	final static private String PAGE="path.page.info";
	final static private String PAGE_ISSURE="path.page.makeIssures";
	


	@Override
	public String execute(HttpServletRequest request, HttpServletResponse respons) {
		
		String page =  ConfigurationManager.getProperty(PAGE);
		String email= (String) request.getParameter(EMAIL);
		String name= (String) request.getParameter(NAME);
		String serName= (String) request.getParameter(SERNAME);
		String passport= (String) request.getParameter(CARD);
		if(!AddIssureLogic.isValidInput(email, passport)){
			page =  ConfigurationManager.getProperty( PAGE_ISSURE);
		}
		else{
			int idHotel=Integer.parseInt( request.getParameter(IDHOTEL));
			int idRoom=Integer.parseInt( request.getParameter(IDROOM));
			int countDays=Integer.parseInt( request.getParameter(MENU));
			int success=AddIssureLogic.addIssure(idRoom, idHotel,  request.getSession().getAttribute(USER).toString(), name, serName, passport, countDays);
			request.setAttribute(ISSADD, success);
		}
		return page;
	}

	@Override
	public boolean changePage(HttpServletResponse response, HttpServletRequest request) {
	
		return false;
	}
}

