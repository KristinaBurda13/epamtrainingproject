package by.epam.bsu.command;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.logic.LoginLogic;

public class GetCitiesCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(GetCitiesCommand.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse respons) {
		return null;
	}

	@Override
	public boolean changePage(HttpServletResponse response,
			HttpServletRequest request) {

		response.setContentType("application/json");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {

			LOG.error("Can not send json string. Error: " + e);
		}
		out.print(LoginLogic.jsonCities().toJSONString());
		out.flush();

		return true;

	}

}
