package by.epam.bsu.command;

import by.epam.sec.action.ActionCommand;


public enum CommandEnum {
	LOGIN {
		{
			this.command = new LoginCommand();
		}
	},
	GETCITIES {
		{
			this.command = new GetCitiesCommand();
		}
	},
	CHANGELANGUAGE{
	
		{
			this.command = new ChangeLanguageCommand();
		}
	},
	MAKEREQUEST {
		{
			this.command = new MakeRequestCommand();
		}
	},
	
	MAINPAGE
	{
		{
			this.command = new MainCommand();
		}
	},
	CHOOSEPAGE
	{
		{
			this.command = new ChooseCommand();
		}
	},
	CHOOSEROOM
	{
		{
			this.command = new ChooseRoomCommand();
		}
	},
	ISSUREPAGE
	{
		{
			this.command = new IssureCommand();
		}
	},
	
	LOGOUT {
		{
			this.command = new LogoutCommand();
		}
	};
	ActionCommand command;

	public ActionCommand getCurrentCommand() {
		return command;
	}
}