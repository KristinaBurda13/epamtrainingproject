package by.epam.sec.logic;

import java.util.Map;
import org.json.simple.JSONObject;
import by.epam.sec.dao.DaoFactory;
import by.epam.sec.dao.RequestDao;
import by.epam.sec.entity.Room;

public class ChooseRoomLogic {
	public static JSONObject returnRoomById(int idRoom, String hotelTitle) {
		DaoFactory factory = DaoFactory.getDAOFactory(DaoFactory.MYSQL);
		RequestDao requestDAO = factory.getAllDAO();
		Room room = requestDAO.reamRoomById(idRoom, hotelTitle);
		Map<String, Object> inMap;
		inMap = room.getMap();
		JSONObject obj = new JSONObject(inMap);
		return obj;
	}

}
