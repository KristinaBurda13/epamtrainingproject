package by.epam.sec.logic;

import java.util.Random;

import by.epam.sec.checker.CheckerRegEx;
import by.epam.sec.dao.DaoFactory;
import by.epam.sec.dao.RequestDao;

public class AddIssureLogic {
	
	private static final int INT_FOR_ID = 1000;
	public static int addIssure(int idRoom, int idHotel, String login,
			String name, String serName, String password, int countDays) {

		DaoFactory factory = DaoFactory.getDAOFactory(DaoFactory.MYSQL);
		RequestDao requestDAO = factory.getAllDAO();
		int idIssure = generateID(idRoom, idHotel);
		int anser = requestDAO.addIssure(idIssure, idRoom, idHotel, login,
				name, serName, password, countDays);
		return anser;
	}

	public static int generateID(int x, int y) {
		Random r = new Random();
		return x * y * r.nextInt(INT_FOR_ID);
	}

	public static boolean isValidInput(String email, String credit) {
		boolean out = true;
		if (!CheckerRegEx.isValidEmail(email)) {
			out = false;
		}
		if (!CheckerRegEx.isValidCredit(credit)) {
			out = false;
		}
		return out;
	}

}
