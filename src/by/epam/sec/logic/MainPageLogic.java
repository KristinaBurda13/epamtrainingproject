package by.epam.sec.logic;

import java.util.ArrayList;

import by.epam.sec.dao.DaoFactory;
import by.epam.sec.dao.RequestDao;
import by.epam.sec.entity.Room;

public class MainPageLogic {

	public static ArrayList<Room> returnRooms(int persons, int classRoom,
			String city) {
		DaoFactory factory = DaoFactory.getDAOFactory(DaoFactory.MYSQL);
		RequestDao requestDAO = factory.getAllDAO();
		ArrayList<Room> listRooms = requestDAO.readSuitableRoom(persons,
				classRoom, city);
		return listRooms;

	}

}
