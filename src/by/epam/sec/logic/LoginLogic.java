package by.epam.sec.logic;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;

import by.epam.sec.aditionalArrays.HotelList;
import by.epam.sec.dao.DaoFactory;
import by.epam.sec.dao.RequestDao;
import by.epam.sec.entity.User;
import by.epam.sec.exception.HotelSqlExeption;

public class LoginLogic {
	public static final Logger LOG = Logger.getLogger(LoginLogic.class);
	private static boolean isAdmin = false;

	public static boolean isAdmin() {
		return isAdmin;
	}

	public static JSONArray jsonCities() {
		JSONArray data1 = new JSONArray();
		ArrayList<String> intStr = returnCities();
		for (int i = 0; i < intStr.size(); i++) {
			data1.add(intStr.get(i));
		}
		return data1;
	}

	public static ArrayList<String> returnCities() {
		HotelList list = null;
		try {
			DaoFactory factory = DaoFactory.getDAOFactory(DaoFactory.MYSQL);
			RequestDao requestDAO = factory.getAllDAO();
			list = requestDAO.readAllHotels();
		} catch (HotelSqlExeption e) {
			LOG.error("Can not read all hotels from MySql db. Error:" + e);

		}
		return list.readAllCities();
	}

	public static boolean checkLogin(String enterLogin, String enterPass) {
		boolean out = false;
		DaoFactory factory = DaoFactory.getDAOFactory(DaoFactory.MYSQL);
		RequestDao requestDAO = factory.getAllDAO();
		ArrayList<User> list = requestDAO.readAllUsers();
		String name;
		String password;
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
			name = list.get(i).getName();
			password = list.get(i).getPassword();
			if (name.equals(enterLogin) && password.equals(enterPass)) {
				isAdmin = list.get(i).isAdmin();
				out = true;
				break;
			}
		}
		return out;
	}
}