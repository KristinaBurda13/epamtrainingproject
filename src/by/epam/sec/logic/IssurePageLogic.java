package by.epam.sec.logic;

import java.util.ArrayList;
import java.util.Map;

import by.epam.sec.dao.DaoFactory;
import by.epam.sec.dao.RequestDao;
import by.epam.sec.entity.Issure;

public class IssurePageLogic {

	public static ArrayList<Map<String, Object>> retrunIssures() {

		DaoFactory factory = DaoFactory.getDAOFactory(DaoFactory.MYSQL);
		RequestDao requestDAO = factory.getAllDAO();
		ArrayList<Issure> list = requestDAO.readeAllIssures();
		ArrayList<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < list.size(); i++) {
			dataList.add(list.get(i).getMap());

		}
		return dataList;
	}

}
