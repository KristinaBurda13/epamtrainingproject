package by.epam.sec.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.sec.exception.HotelSqlExeption;

public class ConnectionPool {
	public static final Logger LOG = Logger.getLogger(ConnectionPool.class);
	static final String propFile = "data/db";
	private static final int DEFAULT_POOL_SIZE = 10;
	private static int MAX_POOL_SIZE = DEFAULT_POOL_SIZE;
	private static Lock lock = new ReentrantLock();
	private static ConnectionPool instance;
	private static BlockingQueue<Connection> queueConnection;
	
	private ConnectionPool(String url, String driver, String user,
			String password, int poolSize) {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			
			LOG.error("Can not load driver");
		}
		queueConnection = new ArrayBlockingQueue<Connection>(poolSize);
		for (int i = 0; i < poolSize; i++) {
			Connection connection = null;
			try {
				connection = DriverManager.getConnection(url, user, password);
			} catch (SQLException e) {
				
				LOG.error("Can not connect to MYSQL db. Error:" + e);
			}
			queueConnection.offer(connection);
		}
	}
	
	public Connection takeConnection() {
		Connection connection = null;
		try {
			connection = queueConnection.take();
		} catch (InterruptedException e) {
			LOG.error("Free connection waiting interrupted. "
					+ "Returned `null` connection.");
		}
		return connection;
	}

	public static ConnectionPool initialize() throws SQLException {
		return initialize(propFile);

	}

	public static ConnectionPool initialize(String propertyFile)  {
		if (instance == null) {
			lock.lock();
			try
			{
				ResourceBundle rb = ResourceBundle.getBundle(propertyFile);
				String DBAddress = rb.getString(("address"));
				String driver = rb.getString("driver");
				String user = rb.getString("user");
				String password = rb.getString("password");
				String poolSizeStr = rb.getString("poolsize");
				int poolSize = DEFAULT_POOL_SIZE;
				if (poolSizeStr != null)
					Integer.parseInt(poolSizeStr);
	
				MAX_POOL_SIZE = poolSize;
	
				LOG.info("Trying to create pool of the connection...");
				instance = new ConnectionPool(DBAddress, driver, user,
						password, poolSize);
				LOG.info("Connection pool succesfully initialized.");
			}finally{
				lock.unlock();
			}
		}
		return  instance;
	}

	public static void dispose() throws HotelSqlExeption {
		if (instance != null) {
			instance.clearConnectionQueue();
			instance = null;
			LOG.info("Connection pool succesfully disposed.");
		}
	}

	public void retriveConnection(Connection connection) {
		try {
			if (!connection.isClosed()) {
				if (!queueConnection.offer(connection)) {
					LOG.info("Connection not added.");
				}
			} else {
				LOG.info("Trying to release closed connection.");
			}
		} catch (SQLException e) {
			
			LOG.info("Error: " + e);
		}
	}
	private void clearConnectionQueue() throws HotelSqlExeption {
		Connection connection;
		while ((connection = queueConnection.poll()) != null) {
			try {
				if (!connection.getAutoCommit()) {
					connection.commit();
				}
				connection.close();
			} catch (SQLException e) {
				LOG.info("Error: " + e);
			}
		}
	}
}
