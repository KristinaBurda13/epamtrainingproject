package by.epam.sec.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import by.epam.sec.exception.HotelSqlExeption;
import by.epam.sec.pool.ConnectionPool;

public class MySqlDaoFactory extends DaoFactory {
	public static final Logger LOG = Logger.getLogger(MySqlDaoFactory.class);
	private static ConnectionPool connectionPool;

	public static Connection takeConnection() throws HotelSqlExeption {
		if (connectionPool == null) {

			try {
				connectionPool = ConnectionPool.initialize();
			} catch (SQLException e) {
				LOG.error("Can not get coonection to MySql DB");
			}
			//
		}
		return connectionPool.takeConnection();
	}

	public static void retriaveConnection(Connection connection) {
		connectionPool.retriveConnection(connection);
	}

	@Override
	public RequestDao getAllDAO() {
		return new MySqlRequestDao();
	}
}