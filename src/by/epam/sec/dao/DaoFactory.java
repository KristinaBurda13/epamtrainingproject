package by.epam.sec.dao;


public abstract class DaoFactory {
	public static final int MYSQL = 1;
	
	public abstract RequestDao getAllDAO();
	
	public static DaoFactory getDAOFactory(int whichFactory) {
		switch (whichFactory) {
		case MYSQL:
			return new MySqlDaoFactory();
		default:
			return null;
		}
	}
}
