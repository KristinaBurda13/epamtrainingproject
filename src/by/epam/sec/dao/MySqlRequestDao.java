package by.epam.sec.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import by.epam.sec.aditionalArrays.HotelList;
import by.epam.sec.entity.Calendar;
import by.epam.sec.entity.Hotel;
import by.epam.sec.entity.Issure;
import by.epam.sec.entity.Room;
import by.epam.sec.entity.User;
import by.epam.sec.exception.HotelSqlExeption;

public class MySqlRequestDao implements RequestDao {

	private static final Logger LOG = Logger.getLogger(MySqlRequestDao.class);
	private static final String QUERY_ALL_HOTEL = "select * from hoteldb.hotel;";
	private static final String QUERY_ALL_USERS = "select * from hoteldb.user;";
	private static final String QUERY_ALL_ADMINS = "select*  from hoteldb.admin;";
	private static final String QUERY_ALL_ISSURES = "SELECT * FROM hoteldb.issure;";
	private static final String QUERY_SUITABLE_ROOMS = "Select * from room inner Join hotel AS temp on hotel_id_hotel = id_hotel join calendar on (id_room = calendar.room_id_room AND id_hotel=room_hotel_id_hotel ) where (  persons=? AND class=? AND  city=?);";
	private static final String QUERY_ID_ROOM = "SELECT * FROM hoteldb.room inner join hoteldb.hotel  as temp on hotel_id_hotel=id_hotel where (title=? AND id_room=? ); ";
	private static final String INSERT_ISSURE = "INSERT INTO hoteldb.issure (id_issure, issure_persons, issure_class, issure_days_in, issure_day_out, user_user_login, room_id_room, room_hotel_id_hotel, name, ser_name, passport, count_days) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

	@Override
	public HotelList readAllHotels() throws HotelSqlExeption {
		ArrayList<Hotel> temp = new HotelList();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		try {
			connection = MySqlDaoFactory.takeConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(QUERY_ALL_HOTEL);

			while (resultSet.next()) {
				int id_hotel = resultSet.getInt("id_hotel");
				String title = resultSet.getString("title");
				String city = resultSet.getString("city");
				int stars = resultSet.getInt("stars");
				String description = resultSet.getString("description");
				temp.add(new Hotel(id_hotel, city, title, stars, description));
			}

		} catch (HotelSqlExeption | SQLException e) {
			LOG.info("Error: " + e);
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				LOG.info("Error: " + e);
			}
			if (connection != null)
				MySqlDaoFactory.retriaveConnection(connection);
		}

		return (HotelList) temp;
	}

	@Override
	public ArrayList<User> readAllUsers() {
		ArrayList<User> temp = new ArrayList<User>();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		try {
			connection = MySqlDaoFactory.takeConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(QUERY_ALL_USERS);

			while (resultSet.next()) {

				String login = resultSet.getString("user_login");
				String password = resultSet.getString("user_password");

				temp.add(new User(login, password, false));
			}

			resultSet = statement.executeQuery(QUERY_ALL_ADMINS);

			while (resultSet.next()) {

				String login = resultSet.getString("admin_login");
				String password = resultSet.getString("login_password");

				temp.add(new User(login, password, true));
			}

		} catch (SQLException | HotelSqlExeption e) {
			LOG.info("Problems with data base. Error: " + e);
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				LOG.error("Error: " + e);
			}
			if (connection != null)
				MySqlDaoFactory.retriaveConnection(connection);
		}

		return temp;
	}

	@Override
	public ArrayList<Room> readSuitableRoom(int inPersins, int inClass,
			String inCity) {
		ArrayList<Room> temp = new ArrayList<Room>();
		PreparedStatement pStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		try {
			connection = MySqlDaoFactory.takeConnection();
			pStatement = connection.prepareStatement(QUERY_SUITABLE_ROOMS);
			pStatement.setInt(1, inPersins);
			pStatement.setInt(2, inClass);
			pStatement.setString(3, inCity);
			boolean isNewRomm;
			resultSet = pStatement.executeQuery();

			while (resultSet.next()) {
				int idHotel = resultSet.getInt("id_hotel");
				int idRoom = resultSet.getInt("id_room");
				int persons = resultSet.getInt("persons");
				int classRoom = resultSet.getInt("class");
				int pricePerNight = resultSet.getInt("price_per_night");
				int idCalendar = resultSet.getInt("id_calendar");
				Date dateIN = resultSet.getDate("date_in");
				GregorianCalendar dateING = new GregorianCalendar();
				dateING.setTime(dateIN);
				Date dateOut = resultSet.getDate("date_out");
				GregorianCalendar dateOutG = new GregorianCalendar();
				dateOutG.setTime(dateOut);
				Calendar cal = new Calendar(idCalendar, dateING, dateOutG);
				isNewRomm = true;

				for (int i = 0; i < temp.size(); i++) {
					if ((idHotel == temp.get(i).getId_hotel())
							&& (idRoom == temp.get(i).getId_room())) {
						temp.get(i).addCalendar(cal);
						isNewRomm = false;
						break;
					}

				}
				if (isNewRomm) {
					Room room = new Room(idRoom, persons, classRoom,
							pricePerNight, idHotel, cal);
					room.setHotelTitle(resultSet.getString("title"));
					temp.add(room);

				}

			}

		} catch (SQLException | HotelSqlExeption e) {
			LOG.info("Problems with data base. Error: " + e);
		} finally {
			try {
				pStatement.close();
			} catch (SQLException e) {
				LOG.info("Error: " + e);
			}
			if (connection != null)
				MySqlDaoFactory.retriaveConnection(connection);
		}

		return temp;
	}

	@Override
	public ArrayList<Issure> readeAllIssures() {
		ArrayList<Issure> temp = new ArrayList<Issure>();
		Statement statement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		try {
			connection = MySqlDaoFactory.takeConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(QUERY_ALL_ISSURES);

			while (resultSet.next()) {
				int idIssure = resultSet.getInt("id_issure");
				int idHotel = resultSet.getInt("room_hotel_id_hotel");
				int issurePersons = resultSet.getInt("issure_persons");
				int issureClass = resultSet.getInt("issure_class");
				Date dateIN = resultSet.getDate("issure_days_in");
				GregorianCalendar dateING = new GregorianCalendar();
				dateING.setTime(dateIN);
				Date dateOut = resultSet.getDate("issure_day_out");
				GregorianCalendar dateOutG = new GregorianCalendar();
				String userLogin = resultSet.getString("user_user_login");

				temp.add(new Issure(idIssure, issurePersons, issureClass,
						dateING, dateOutG, userLogin, idHotel));
			}

		} catch (SQLException | HotelSqlExeption e) {
			LOG.info("Problems with data base. Error: " + e);
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				LOG.info("Error: " + e);
			}
			if (connection != null)
				MySqlDaoFactory.retriaveConnection(connection);
		}

		return temp;
	}

	@Override
	public Room reamRoomById(int idRoom, String hotelTitle) {
		Room temp = new Room();
		PreparedStatement pStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;

		try {
			connection = MySqlDaoFactory.takeConnection();
			pStatement = connection.prepareStatement(QUERY_ID_ROOM);
			pStatement.setString(1, hotelTitle);
			pStatement.setInt(2, idRoom);

			resultSet = pStatement.executeQuery();

			while (resultSet.next()) {
				temp.setId_room(idRoom);
				temp.setId_hotel(resultSet.getInt("id_hotel"));
				temp.setClassRoom(resultSet.getInt("class"));
				temp.setHotelDescription(resultSet.getString("description"));
				temp.setHotelTitle(hotelTitle);
				temp.setPrice_per_nighr(resultSet.getInt("price_per_night"));

			}

		} catch (SQLException | HotelSqlExeption e) {
			LOG.info("Problems with data base " + e);
		} finally {
			try {
				pStatement.close();
			} catch (SQLException e) {
				LOG.info("Error: " + e);
			}
			if (connection != null)
				MySqlDaoFactory.retriaveConnection(connection);
		}

		return temp;
	}

	@Override
	public int addIssure(int idIssure, int idRoom, int idHotel, String login,
			String name, String serName, String passport, int countDays) {

		PreparedStatement pStatement = null;

		Connection connection = null;
		int retrunStatment = 0;

		try {
			try {
				Class.forName("org.gjt.mm.mysql.Driver").newInstance();

				connection = MySqlDaoFactory.takeConnection();
				pStatement = connection.prepareStatement(INSERT_ISSURE);
				pStatement.setInt(1, idIssure);
				pStatement.setInt(2, 2);
				pStatement.setInt(3, 5);
				pStatement.setDate(4, new Date(2015, 04, 07));
				pStatement.setDate(5, new Date(2015, 04, 07));
				pStatement.setString(6, login);
				pStatement.setInt(7, idRoom);
				pStatement.setInt(8, idHotel);
				pStatement.setString(9, name);
				pStatement.setString(10, serName);
				pStatement.setString(11, passport);
				pStatement.setInt(12, countDays);

				retrunStatment = pStatement.executeUpdate();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | HotelSqlExeption e) {
				LOG.info(" Cant dind driver: org.gjt.mm.mysql.Driver Error: "
						+ e);

			}

		} catch (SQLException e) {
			LOG.info("Problems with data base .Error: " + e);
		} finally {
			try {
				pStatement.close();
			} catch (SQLException e) {
				LOG.info("Error: " + e);
			}
			if (connection != null)
				MySqlDaoFactory.retriaveConnection(connection);
		}

		return retrunStatment;
	}

}
