package by.epam.sec.dao;

import java.util.ArrayList;

import by.epam.sec.aditionalArrays.HotelList;
import by.epam.sec.entity.Issure;
import by.epam.sec.entity.Room;
import by.epam.sec.entity.User;
import by.epam.sec.exception.HotelSqlExeption;

public interface RequestDao {

	public HotelList readAllHotels() throws HotelSqlExeption;

	public ArrayList<User> readAllUsers();

	public ArrayList<Room> readSuitableRoom(int inPersins, int InClass,
			String inCity);

	public ArrayList<Issure> readeAllIssures();

	public Room reamRoomById(int idRoom, String hotelTitle);

	public int addIssure(int idIssure, int idRoom, int idHotel, String login,
			String name, String serName, String password, int countDays);

}