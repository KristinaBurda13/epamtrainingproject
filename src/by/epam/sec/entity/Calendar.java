package by.epam.sec.entity;

import java.util.GregorianCalendar;

public class Calendar {
	
	private int id_calendar;
	private GregorianCalendar dateIN;
	private GregorianCalendar dateOut;
	
	public Calendar(int id_calendar, GregorianCalendar dateIN,
			GregorianCalendar dateOut) {
		super();
		this.id_calendar = id_calendar;
		this.dateIN = dateIN;
		this.dateOut = dateOut;
	}
	
	public int getId_calendar() {
		return id_calendar;
	}
	public void setId_calendar(int id_calendar) {
		this.id_calendar = id_calendar;
	}
	public GregorianCalendar getDateIN() {
		return dateIN;
	}
	public void setDateIN(GregorianCalendar dateIN) {
		this.dateIN = dateIN;
	}
	public GregorianCalendar getDateOut() {
		return dateOut;
	}
	public void setDateOut(GregorianCalendar dateOut) {
		this.dateOut = dateOut;
	}

	@Override
	public String toString() {
		return "Calendar [id_calendar=" + id_calendar + ", dateIN=" + dateIN
				+ ", dateOut=" + dateOut + "]";
	}
	
	
	
	

}
