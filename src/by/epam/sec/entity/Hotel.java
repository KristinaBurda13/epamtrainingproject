package by.epam.sec.entity;

public class Hotel {

	
	int idHotel;
	String city;
	String title;
	int stars;
	String description;
	public Hotel(int idHotel, String city, String title, int stars,
			String description) {
		super();
		this.idHotel = idHotel;
		this.city = city;
		this.title = title;
		this.stars = stars;
		this.description = description;
	}
	
    public int getIdHotel() {
		return idHotel;
	}
	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Hotel [idHotel=" + idHotel + ", city=" + city + ", title="
				+ title + ", stars=" + stars + ", description=" + description
				+ "]";
	}
}
