package by.epam.sec.entity;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class Issure {
	
	
	private int idIssure;
	private int issurePersons;
	private int issureSlass;
	private GregorianCalendar issureDaysIn;
	private GregorianCalendar issureDaysOut;
	private String userLogin;
	private int idHotel;
	
	public Issure(int idIssure, int issurePersons, int issureSlass,
			GregorianCalendar issureDaysIn, GregorianCalendar issureDaysOut,
			String userLogin, int idHotel) {
		super();
		this.idIssure = idIssure;
		this.issurePersons = issurePersons;
		this.issureSlass = issureSlass;
		this.issureDaysIn = issureDaysIn;
		this.issureDaysOut = issureDaysOut;
		this.userLogin = userLogin;
		this.idHotel = idHotel;
	}
	
	public int getIdIssure() {
		return idIssure;
	}
	public void setIdIssure(int idIssure) {
		this.idIssure = idIssure;
	}
	public int getIssurePersons() {
		return issurePersons;
	}
	public void setIssurePersons(int issurePersons) {
		this.issurePersons = issurePersons;
	}
	public int getIssureSlass() {
		return issureSlass;
	}
	public void setIssureSlass(int issureSlass) {
		this.issureSlass = issureSlass;
	}
	public GregorianCalendar getIssureDaysIn() {
		return issureDaysIn;
	}
	public void setIssureDaysIn(GregorianCalendar issureDaysIn) {
		this.issureDaysIn = issureDaysIn;
	}
	public GregorianCalendar getIssureDaysOut() {
		return issureDaysOut;
	}
	public void setIssureDaysOut(GregorianCalendar issureDaysOut) {
		this.issureDaysOut = issureDaysOut;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public int getIdHotel() {
		return idHotel;
	}
	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	
	public Map<String, Object> getMap()
	{
		Map<String,Object> out= new HashMap<String, Object>();
		out.put("id_issure", this.getIdIssure());
		out.put("issure_persons", this.getIssurePersons());
		out.put("issure_class", this.getIssureSlass());
		out.put("issure_days_in", this.getIssureDaysIn().toZonedDateTime());
		out.put("issure_days_out", this.getIssureDaysOut().toZonedDateTime());
		out.put("userLogin", this.getUserLogin());
		out.put("id_hotel", this.getIdHotel());
		return out;
	}

	@Override
	public String toString() {
		return "Issure [idIssure=" + idIssure + ", issurePersons="
				+ issurePersons + ", issureSlass=" + issureSlass
				+ ", issureDaysIn=" + issureDaysIn + ", issureDaysOut="
				+ issureDaysOut + ", userLogin=" + userLogin + ", idHotel="
				+ idHotel + "]";
	}
	
	
	

}
