package by.epam.sec.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Room {
	
	
	private int id_room;
	private int persons;
	private int classRoom;
	private int price_per_nighr;
	
	private int id_hotel;
	private String hotelTitle;
	private String hotelDescription;
	private ArrayList<Calendar> calendarList = new ArrayList<Calendar>();
	
	public Room()
	{
		
	}
	
	public Room(int id_room, int persons, int classRoom, int price_per_nighr,
			int id_hotel,Calendar calendarList) {
		super();
		this.id_room = id_room;
		this.persons = persons;
		this.classRoom = classRoom;
		this.price_per_nighr = price_per_nighr;
		this.id_hotel = id_hotel;
		this.calendarList.add( calendarList);
	}
	public void addCalendar(Calendar cal)
	{
		this.calendarList.add(cal);
	}
	public int getId_room() {
		return id_room;
	}
	public void setId_room(int id_room) {
		this.id_room = id_room;
	}
	public int getPersons() {
		return persons;
	}
	public void setPersons(int persons) {
		this.persons = persons;
	}
	public int getClassRoom() {
		return classRoom;
	}
	public void setClassRoom(int classRoom) {
		this.classRoom = classRoom;
	}
	public int getPrice_per_nighr() {
		return price_per_nighr;
	}
	public void setPrice_per_nighr(int price_per_nighr) {
		this.price_per_nighr = price_per_nighr;
	}
	public int getId_hotel() {
		return id_hotel;
	}
	public void setId_hotel(int id_hotel) {
		this.id_hotel = id_hotel;
	}
	public ArrayList<Calendar> getCalendarList() {
		return calendarList;
	}
	public void setCalendarList(ArrayList<Calendar> calendarList) {
		this.calendarList = calendarList;
	}
	
	public Map<String, Object> getMap()
	{
		Map<String, Object> out= new HashMap<String,Object>();
		out.put("id_room", this.getId_room());
		out.put("class_room", this.getClassRoom());
		out.put("persons", this.getPersons());
		out.put("id_hotel", this.getId_hotel());
		out.put("price_per_night", this.getPrice_per_nighr());
		out.put("hotel_title",this.getHotelTitle());
		out.put("hotel_description", this.getHotelDescription());
		return out;
	}
	
	@Override
	public String toString() {
		return "Room [id_room=" + id_room + ", persons=" + persons
				+ ", classRoom=" + classRoom + ", price_per_nighr="
				+ price_per_nighr + ", id_hotel=" + id_hotel
				+ ", calendarList=" + calendarList + "]";
	}

	public String getHotelTitle() {
		return hotelTitle;
	}

	public void setHotelTitle(String hotelTitle) {
		this.hotelTitle = hotelTitle;
	}

	public String getHotelDescription() {
		return hotelDescription;
	}

	public void setHotelDescription(String hotelDescription) {
		this.hotelDescription = hotelDescription;
	}
	
	

}
