package by.epam.sec.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.sec.action.ActionCommand;
import by.epam.sec.action.ActionFactory;
import by.epam.sec.resourse.ConfigurationManager;
import by.epam.sec.resourse.MessageManager;

@WebServlet("/controller")
public class Controller extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);

	}

	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String page = null;

		ActionFactory client = new ActionFactory();
		ActionCommand command = client.defineCommand(request);
		if (!command.changePage(response, request)) {
			page = command.execute(request, response);
			if (page != null) {
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher(page);
				dispatcher.forward(request, response);
			} else {
				page = ConfigurationManager.getProperty("path.page.index");
				request.getSession().setAttribute("nullPage",
						MessageManager.getProperty("message.nullpage"));
				response.sendRedirect(request.getContextPath() + page);
			}
		}
	}
}