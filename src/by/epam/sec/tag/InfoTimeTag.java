package by.epam.sec.tag;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class InfoTimeTag extends TagSupport {
	@Override
	public int doStartTag() throws JspException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
		Date currentDate = new Date();
		String date = sdf.format(currentDate);
		try {
			JspWriter out = pageContext.getOut();
			out.write(date);
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
}
