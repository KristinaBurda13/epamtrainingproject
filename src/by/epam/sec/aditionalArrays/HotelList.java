package by.epam.sec.aditionalArrays;

import java.util.ArrayList;

import by.epam.sec.entity.Hotel;

public class HotelList extends ArrayList<Hotel> {

	public ArrayList<String> readAllCities() {
		ArrayList<String> cities = new ArrayList<String>();

		for (int i = 0; i < this.size(); i++) {
			if (!cities.contains(this.get(i).getCity())) {
				cities.add(this.get(i).getCity());
			}
		}
		return cities;

	}

}
