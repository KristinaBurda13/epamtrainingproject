package by.epam.sec.exception;

import java.sql.SQLException;

public class HotelSqlExeption extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6402065639634845951L;

	public HotelSqlExeption(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super();
		
	}

	public HotelSqlExeption(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public HotelSqlExeption(String arg0) {
		super(arg0);
		
	}

	public HotelSqlExeption(Throwable arg0) {
		super(arg0);
		
	}

	public HotelSqlExeption() {
		super();
		
	}

	
}
