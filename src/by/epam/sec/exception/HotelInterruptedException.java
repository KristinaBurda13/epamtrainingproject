package by.epam.sec.exception;

public class HotelInterruptedException extends Exception {

	public HotelInterruptedException() {
		super();

	}

	public HotelInterruptedException(String s) {
		super(s);

	}

}
