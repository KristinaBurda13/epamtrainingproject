package by.epam.sec.exception;

import java.io.IOException;


public class HotelIoException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -299266215724757761L;

	public HotelIoException() {
		super();
		
	}

	public HotelIoException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public HotelIoException(String message) {
		super(message);
		
	}

	public HotelIoException(Throwable cause) {
		super(cause);
		
	}

	

}
