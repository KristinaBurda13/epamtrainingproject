package by.epam.sec.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ActionCommand {
	public String execute(HttpServletRequest request, HttpServletResponse respons);
	public boolean changePage(HttpServletResponse response, HttpServletRequest request) ;
}