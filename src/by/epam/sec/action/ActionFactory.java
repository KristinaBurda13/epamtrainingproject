package by.epam.sec.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.bsu.command.CommandEnum;
import by.epam.bsu.command.EmptyCommand;
import by.epam.sec.resourse.MessageManager;

public class ActionFactory {
	private static final String COMMAND = "command";
	private static final String MESSAGE = "message.wrongaction";
	private static final String ATTRIBUTE = "wrongAction";

	private static final Logger LOG = Logger.getLogger(ActionFactory.class);

	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand current = new EmptyCommand();
		String action = request.getParameter(COMMAND);
		System.out.println("Command" +action);

		if (action == null || action.isEmpty()) {
			return current;
		}

		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();

		} catch (IllegalArgumentException e) {
			request.setAttribute(ATTRIBUTE,
					action + MessageManager.getProperty(MESSAGE));
			LOG.info(MESSAGE + MessageManager.getProperty(MESSAGE));
		}
		return current;
	}
}
