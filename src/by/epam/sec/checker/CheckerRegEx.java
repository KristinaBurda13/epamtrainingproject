package by.epam.sec.checker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckerRegEx {

	private static final String EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String CREDIT = "[0-9]{13,16}";
	private static final String LOGIN = "^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$";// До
																			// 20
																			// символов,
																			// 1ый
																			// -
																			// обязательно
																			// буква
	private static final String PASSWORD = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$";// Строчные
																							// и
																							// прописные
																							// буквы,
																							// цифры
	private static Pattern pattern;
	private static Matcher matcher;

	public static boolean isValidEmail(String inEmail) {
		pattern = Pattern.compile(EMAIL);
		matcher = pattern.matcher(inEmail);
		return matcher.matches();

	}

	public static boolean isValidCredit(String inCredit) {
		pattern = Pattern.compile(CREDIT);
		matcher = pattern.matcher(inCredit);
		return matcher.matches();

	}

	public static boolean isValidLogin(String userName) {
		pattern = Pattern.compile(LOGIN);
		matcher = pattern.matcher(userName);
		return matcher.matches();

	}

	public static boolean isValidPassword(String password) {
		pattern = Pattern.compile(PASSWORD);
		matcher = pattern.matcher(password);
		return matcher.matches();

	}

}
