<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:bundle basename="data.pagecontent" prefix="label.">



<html>

	<head>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/WebContent/WEB-INF/lib/jquery-2.1.3.min.js"></script>
		
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script
			src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script type="text/javascript"
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
		<link rel="stylesheet"
			href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		
		
		
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
	
	</head>
<script type="text/javascript">
	
	$(function() {
	$('#menuLang').change(function(){
		  var value = $(this).val() || $(this).text();
		  $.ajax({
				url : "Controller?command=changelanguage",
				type : 'POST',
				
				data : {
					'lang' : value
				},
				success : function() {
					location.reload()
				}
				
			});
		});
	});
</script>
	
<body class="container-fluid">

	<form name="MainForm" method="POST" action="controller">
		<input type="hidden" name="command" value="navbar" />
			<nav class="navbar navbar-default">
			  <div class="divider-vertical">
			    <div class="navbar-header">
			      <a class="navbar-brand" href="#"><fmt:message key="siteName"/></a>
			    </div>
			    <div>
			      <ul class="nav navbar-nav">
			        <li ><a href="#"><fmt:message key="choose"/></a></li> 
			        <li ><a href="#"><fmt:message key="contacts"/></a></li> 
			       </ul>
			        
			     	<ul class="nav navbar-nav navbar-right">
            			<li ><a><span class="glyphicon glyphicon-calendar"></span><ctg:info-time/></a></li>
			       		<li ><a href="login.jsp"><span class="glyphicon glyphicon-user"></span>${user}</a></li>
			        	<li ><label for="inputDate"><fmt:message key="lang"/></label>
    						<select id="menuLang" class=" selectpicker" name="menuLang" >
    							<option value="null">ChooseLang</option>
								<option value="en_US" >English</option>
								<option value="ru_RU" >Pусский</option>
							</select>
						</li>
        			</ul>
			     </div>
			    </div>
			</nav>
		</form>
		</body>
	</html>
</fmt:bundle>