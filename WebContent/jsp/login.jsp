
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="/jsp/navbar.jsp"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<fmt:setLocale value="${lang}" scope="session" />
	<fmt:bundle basename="data.pagecontent" prefix="label.">
	

<html>
<head>
	<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body>
<form name="loginForm" method="POST" action="controller">

		
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-offset-5 col-md-3">
            <div class="form-login">
            <h4><fmt:message key="welcome" /></h4>
            <input type="hidden" name="command" value="login" />
            <input type="text" name="login" value="" class="form-control input-sm chat-input"/>
            </br>
            <input type="password" name="password"  value="" class="form-control input-sm chat-input" />
	            ${errorLoginPassMessage}
				<br/>
				${wrongAction}
				<br/>
				${nullPage}

			<br/>
            <div class="wrapper">
            <span class="group-btn">     
                <input type="submit" value="Log in" class="btn btn-primary btn-md"> <i class="fa fa-sign-in"></i></a>
            </span>
            </div>
            </div>
        
        </div>
    </div>
</div>
	</form>
</body>
</html>
</fmt:bundle>

