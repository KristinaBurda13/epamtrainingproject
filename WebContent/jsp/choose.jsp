<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="/jsp/navbar.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:bundle basename="data.pagecontent" prefix="label.">


<html>
	<head>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/WebContent/WEB-INF/lib/jquery-2.1.3.min.js"></script>
		
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script
			src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script type="text/javascript"
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
		<link rel="stylesheet"
			href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Choose hotel</title>
	</head>
	<script type="text/javascript">
	function getCityOptions(arrCity, valor_hotel, valor_room) {

		var resultHtml = " <li>" + "Информация о выбранном отеле" + "</li>";

		resultHtml += " <li>" + "Hotel:" + arrCity.hotel_title + "</li>";
	    resultHtml += " <li>" + arrCity.hotel_description + "</li>";
		resultHtml += " <li>" + "Price for 1 night:" + arrCity.price_per_night + "</li>";

		resultHtml += "<input type='hidden' name='title_of_choosen_hotel' value='";
			resultHtml +=valor_hotel;
			resultHtml +="'/>";
		resultHtml += "<input type='hidden' name='id_of_choosen_room' value='";
			resultHtml +=arrCity.id_room;
			resultHtml +="'/>";
		resultHtml += "<span class='roup-btn'> <input type='submit' value='Make a request'class='btn btn-primary btn-md'> <i class='fa fa-sign-in'></i></a></span>";
		return resultHtml;

	}

	$(function() {
		$('tr').click(
				function() {
					var valor_hotel = $($(this).find('td')[0]).html();
					var valor_room = $($(this).find('td')[1]).html();
					$.ajax({
						url : "Controller?command=choosepage",
						type : 'POST',
						data : {
							'id_room' : valor_room,
							'hotel_title' : valor_hotel
						},
						success : function(data) {
							$('#menu').html(
									getCityOptions(data, valor_hotel,
											valor_room));
}
					});
			});
	});
</script>
<body class="container-fluid">
	<form name="MainForm" method="POST" action="controller">
		<input type="hidden" name="command" value="chooseroom" />
				<h2>
					<fmt:message key="selectRoom" />
				</h2>
				<p>
					<fmt:message key="listRooms" />
				</p>
			<div id="dialog" title="Basic dialog"></div>
			<div class="table-responsive">
				<table class="table table-hover" id="myTable">
					<thead>
						<tr>
							<th><fmt:message key="Hotel" /></th>
							<th><fmt:message key="Class" /></th>
							<th><fmt:message key="Persons" /></th>
							<th><fmt:message key="Price_per_night" /></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						
						<c:forEach items="${suitable_rooms}" var="name">
							<tr>
								<td><c:out value="${ name.hotel_title }" /></td>
								<td><c:out value="${ name.id_room }" /></td>
								<td><c:out value="${ name.class_room}" /></td>
								<td><c:out value="${ name.persons}" /></td>
								<td><c:out value="${ name.price_per_night}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<ul name="menu" id="menu" class="list-group">
					<li></li>
				</ul>
			</div>
		</form>
	</body>
</html>
</fmt:bundle>