<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="/jsp/navbar.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:bundle basename="data.pagecontent" prefix="label.">


<html>
	<head>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/WebContent/WEB-INF/lib/jquery-2.1.3.min.js"></script>
		
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script
			src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script type="text/javascript"
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
		<link rel="stylesheet"
			href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		
		
		
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Choose</title>
	
	</head>
	<body class="container-fluid">
		<form name="MainForm" method="POST" action="controller">
			<input type="hidden" name="command" value="infopage" />
				 <div class="span8">
				<c:set var="isOk" value="${isAddIssure}" />
				<c:choose> 
	  				<c:when test="${isOk=='0'}">
	   					<h1> <fmt:message key="notAcceptIssure" /></h1>
	  				</c:when>
				  	<c:otherwise>
				    	<h1><fmt:message key="acceptIssure" /></h1>
				 	 </c:otherwise>
				</c:choose>
	  				 <p>
					    <a class="btn btn-primary btn-large"></a>
					  </p>
					</div>
			</form>
	</body>
</html>
</fmt:bundle>