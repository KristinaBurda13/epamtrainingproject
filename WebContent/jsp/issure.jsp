<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="/jsp/navbar.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:bundle basename="data.pagecontent" prefix = "label." >


<html>

	<head>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/WebContent/WEB-INF/lib/jquery-2.1.3.min.js"></script>
		
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script
			src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<title>Welcome</title>
	</head>
	<body class="container-fluid">
		<form name="MainForm" method="POST" action="controller">
 			<input type="hidden" name="command" value="issurepage" />
				  <h2><fmt:message key="welcome"/>,"${user}"  </h2>
				  <p><fmt:message key="curent_issure"/></p>            
				  <table class="table">
				    <thead>
				      <tr>
				        <th><fmt:message key="Customer"/></th>
				        <th><fmt:message key="Hotel"/></th>
				        <th><fmt:message key="Start"/></th>
				        <th><fmt:message key="End"/></th>
				      </tr>
				    </thead>
				    <tbody>
					   	<c:forEach items="${issures}" var="name">
					       <tr>
						        <td><c:out value="${ name.user_login}"/></td>
						        <td><c:out value="${ name.id_hotel}"/></td>
						        <td><c:out value="${ name.issure_days_in}"/></td>
						        <td><c:out value="${ name.issure_days_out}"/></td>
						        
					        </tr>
				       </c:forEach>
			  		 </tbody>
			 	 </table>
			</form>
		</body>
	</html>
</fmt:bundle>