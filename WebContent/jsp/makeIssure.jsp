<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="/jsp/navbar.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:bundle basename="data.pagecontent" prefix="label.">
<html>

	<head>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/WebContent/WEB-INF/lib/jquery-2.1.3.min.js"></script>
		
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script
			src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script type="text/javascript"
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
		<link rel="stylesheet"
			href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		
		
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Make Issure</title>
	</head>
<script type="text/javascript">
(function( $ ){

	$(function() {

	  $('.rf').each(function(){
	    
		var form = $(this),
	        btn = form.find('.btn_submit');

	    
		form.find('.rfield').addClass('empty_field');

	    
	    function checkInput(){
	      form.find('.rfield').each(function(){
	        if($(this).val() != ''){
	         
			$(this).removeClass('empty_field');
	        } else {
	      
			$(this).addClass('empty_field');
	        }
	      });
	    }

	    
	    function lightEmpty(){
	      form.find('.empty_field').css({'border-color':'#d851564d'});
	      
	      setTimeout(function(){
	        form.find('.empty_field').removeAttr('style');
	      },50000);
	    }

	    
	    setInterval(function(){
	     
		  checkInput();
	      
	      var sizeEmpty = form.find('.empty_field').size();
	      
	      if(sizeEmpty > 0){
	        if(btn.hasClass('disabled')){
	          return false
	        } else {
	          btn.addClass('disabled')
	        }
	      } else {
	        btn.removeClass('disabled')
	      }
	    },500);

	   
	    btn.click(function(){
	      if($(this).hasClass('disabled')){
	        
			lightEmpty();
	        return false
	      } else {
	       
	        form.submit();
	      }
	    });
	  });
	});

	})( jQuery );
	
</script>
<body class="container-fluid">
	<form role="form" name="MainForm" class="rf" id="MainForm" method="POST"
		action="controller">
		<input type="hidden" name="command" value="makerequest" />

		<h2>
			<fmt:message key="selectRoom" />
		</h2>
		<p>
			<fmt:message key="listRooms" />
		</p>
		<div class="form-group">
			<div class="col-xs-6">
				<ul class="list-group">
					<li class="list-group-item"><input type="hidden"
						name="id_hotel" value="${final_info.id_hotel}" /> <fmt:message key="Hotel"/>
						${final_info.hotel_title}</li>
					<li class="list-group-item"><fmt:message key="Class"/> ${final_info.class_room}</li>
					<li class="list-group-item"><fmt:message key="Persons"/>
						${final_info.persons}</li>
					<li class="list-group-item"><fmt:message key="Start"/></li>
					<li class="list-group-item"><fmt:message key="days"/><select
						name="menu1" size="1">

							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
							<option>11</option>

					</select></li>
					<li class="list-group-item"><input type="hidden"
						name="id_room" value="${final_info.id_room}" /> Номер
						${final_info.id_room}</li>
				</ul>
			</div>
		</div>

		<div class="col-xs-6">


			<div class="form-group">
				<label for="exampleInputEmail1"><fmt:message key="email"/></label> <input type="email"
					class="form-control rfield" id="inputEmail" name="inputEmail"
					placeholder="Enter email" value="">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1"><fmt:message key="name"/></label> <input type="text"
					title="jdkfgj" class="form-control rfield" name="issureName"
					data-target="#myModal" id="issureName" placeholder="Name" value="">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1"><fmt:message key="serName"/></label> <input
					type="text" class="form-control rfield" name="issureSerName"
					placeholder="Ser Name" value="">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1"><fmt:message key="card"/></label> <input
					type="text" class="form-control rfield" name="issurePasport"
					placeholder="redir card number" value="">
			</div>
			<span class="group-btn"> <input type="submit" id='submit'
				value="Make request" class="btn btn-primary btn-md btn_submit disabled"> <i
				class="fa fa-sign-in"></i>
			</span>

		</div>
	</form>

	
</body>
	</html>
</fmt:bundle>