<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="en_US" scope="session" />
<fmt:bundle basename="data.pagecontent" prefix = "label." >
	<html>
	<head>
		<title>Error Page</title>
	</head>
	<body>
		Request from ${pageContext.errorData.requestURI} is failed
		<br /> Servlet name or type: ${pageContext.errorData.servletName}
		<br /> Status code: ${pageContext.errorData.statusCode}
		<br /> Exception: ${pageContext.errorData.throwable}
	</body>
	</html>
</fmt:bundle>