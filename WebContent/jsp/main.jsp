<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="/jsp/navbar.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:bundle basename="data.pagecontent" prefix="label.">


<html>

	<head>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/WebContent/WEB-INF/lib/jquery-2.1.3.min.js"></script>
		
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet"
			href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script
			src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script type="text/javascript"
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
			<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
			<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
			<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		
		
		
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Welcome</title>
	</head>

	<script type="text/javascript">
		
		function getCityOptions(arrCity) {
	
			var resultHtml = "";
			for (var i = 0; i < arrCity.length; i++) {
	
					resultHtml += "<option>"  + arrCity[i]+ "</option>";
	
				}
			return resultHtml;
	
		}
		$(document).ready(function() {
	
		$.ajax({
	
			url : "Controller?command=getcities",
			dataType : 'json',
			type: "POST",
	
			success : function(responce) {
	
				$('#menu').html(getCityOptions(responce));
				
			}
	
		});
		});
	</script>

		<body class="container-fluid">
			<h3>
				<fmt:message key="welcome" />
				,${user}
			</h3>
			<h3 id="inner"></h3>
			<form name="MainForm" method="POST" action="controller">
				<input type="hidden" name="command" value="mainpage" />
		
				<div class="form-group">
					<label for="inputDate"></label> 
					<select name="menu"  id="menu" size="">
						<option value="-1"><fmt:message key="choose_city" /></option>
					</select>
				</div>
				<div class="form-group">
					<label for="inputDate"><fmt:message key="Start" /></label> <input
						type="date" name="inDate" class="form-control">
				</div>
				<div class="form-group">
					<label for="inputDate"><fmt:message key="End" /></label> <input
						type="date" name="outDate" class="form-control">
				</div>
		
				<div class="form-group">
					<label for="inputDate"><fmt:message key="Persons" /></label> 
					<select name="menu1" size="1">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
				</div>
				<span class="group-btn"> <input type="submit" value="acsept"
					class="btn btn-primary btn-md"> <i class="fa fa-sign-in"></i>
				</span>
			</form>
		</body>
</html>
</fmt:bundle>